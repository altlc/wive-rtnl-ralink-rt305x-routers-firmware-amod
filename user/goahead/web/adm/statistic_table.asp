<table class="form">
  <tbody>
  <hr>
  <tr id="wirelessAbout" style="display:none;">
    <td class="title" colspan="6">Wireless About</td>
  </tr>
  <tr id="wirelessDriverVersion" style="display:none;">
    <td class="head">Driver Version</td>
    <td colspan="5"><% getStaDriverVer(); %></td>
  </tr>
  <tr id="wirelessMacAddr" style="display:none;">
    <td class="head">Mac Address</td>
    <td colspan="5"><% getStaMacAddrw(); %></td>
  </tr>
  
  <!-- =================  MEMORY  ================= -->
  <table class="form">
    <tr>
      <td class="title" colspan="5" id="statisticMM">Memory</td>
    </tr>
    <tr>
      <td class="head" id="statisticMMTotal" >Memory total: </td>
      <td><% getMemTotalASP(); %></td>
    </tr>
    <tr>
      <td class="head" id="statisticMMLeft" >Memory left: </td>
      <td><% getMemLeftASP(); %></td>
    </tr>
    <tr>
      <td class="title" colspan="2" id="statisticCPU">CPU</td>
    </tr
>
    <tr>
      <td class="head" id="statisticCPUUsage" >CPU usage: </td>
      <td><% getCpuUsageASP(); %></td>
    </tr>
  </table>
  <!-- =================  WAN/LAN  ================== -->
  
  <table class="form"

  <tr>
    <td class="title" colspan="6" id="statisticWANLAN">WAN/LAN</td>
  </tr>
  <tr>
    <td class="head" id="statisticWANRxPkt">WAN Rx packets: </td>
    <td colspan="5"><% getWANRxPacketASP(); %></td>
  </tr>
  <tr>
    <td class="head" id="statisticWANRxBytes">WAN Rx bytes: </td>
    <td colspan="5"><% getWANRxByteASP(); %></td>
  </tr>
  <tr>
    <td class="head" id="statisticWANTxPkt">WAN Tx packets: </td>
    <td colspan="5"><% getWANTxPacketASP(); %></td>
  </tr>
  <tr>
    <td class="head" id="statisticWANTxBytes">WAN Tx bytes: </td>
    <td colspan="5"><% getWANTxByteASP(); %></td>
  </tr>
  <tr>
    <td class="head" id="statisticLANRxPkt">LAN Rx packets: &nbsp; &nbsp; &nbsp; &nbsp;</td>
    <td colspan="5"><% getLANRxPacketASP(); %></td>
  </tr>
  <tr>
    <td class="head" id="statisticLANRxBytes">LAN Rx bytes: </td>
    <td colspan="5"><% getLANRxByteASP(); %></td>
  </tr>
  <tr>
    <td class="head" id="statisticLANTxPkt">LAN Tx packets: </td>
    <td colspan="5"><% getLANTxPacketASP(); %></td>
  </tr>
  <tr>
    <td class="head" id="statisticLANTxBytes">LAN Tx bytes: </td>
    <td colspan="5"><% getLANTxByteASP(); %></td>
  </tr>
  
  <!-- =================  ALL  ================= -->
  <tr>
    <td class="title" colspan="6" id="statisticAllIF">All interfaces</td>
  <tr>
  <tr>
    <th colspan="2">Name</th>
    <th>Rx Packets</th>
    <th>Rx Bytes</th>
    <th>Tx Packets</th>
    <th>Tx Bytes</th>
  </tr>
  <% getAllNICStatisticASP(); %>
  </script>
  
</table>
</tbody>
</table>
