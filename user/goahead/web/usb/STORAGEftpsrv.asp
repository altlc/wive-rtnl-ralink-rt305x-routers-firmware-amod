<!DOCTYPE html>
<html>
<head>
<title>FTP Settings</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate, post-check=0, pre-check=0">
<meta http-equiv="Pragma" content="no-cache">
<script type="text/javascript" src="/lang/b28n.js"></script>
<link rel="stylesheet" href="/style/normal_ws.css" type="text/css">
<link rel="stylesheet" href="/style/controls.css" type="text/css">
<script type="text/javascript" src="/js/controls.js"></script>
<script type="text/javascript" src="/js/validation.js"></script>
<script language="JavaScript" type="text/javascript">
Butterlate.setTextDomain("usb");

var formftp = document.storageftp;
function initValue()
{
	var formftp = document.storageftp;
	var formdlna = document.storagedlna;
	var ftpenabled = '<% getCfgZero(1, "RemoteFTP"); %>';
	formftp.ftp_enabled.value = (ftpenabled == '1') ? '1' : '0';
	formftp.ftp_access.value = '<% getCfgZero(1, "FtpAccess"); %>';
	var dlnaenabled = '<% getCfgZero(1, "DLNAEnabled"); %>';
	formdlna.dlna_enabled.value = (dlnaenabled == '1') ? '1' : '0';
	formdlna.DBUpdate.value = '<% getCfgZero(1, "DLNAUpdType"); %>';
	FtpEnableSwitch(document.storageftp);
	DlnaEnableSwitch(document.storagedlna);
}

function FtpEnableSwitch(form)
{
	var formftp = document.storageftp;
	disableElement( [ formftp.ftp_access, formftp.ftp_port, formftp.ftp_rootdir,
	formftp.ftp_idle_timeout ], formftp.ftp_enabled.value == '0');
}

function DlnaEnableSwitch(form)
{
	var formdlna = document.storagedlna;
	disableElement( [ formdlna.DBUpdate, formdlna.Status, formdlna.dlnarestart ], formdlna.dlna_enabled.value == '0');
}

function submit_apply(parm)
{		
		var formdlna = document.storagedlna;
		formdlna.hiddenButton.value = parm;
		formdlna.submit();	
}

function CheckValue(form)
{
	var formftp = document.storageftp;
	if (formftp.ftp_enabled.value == '1')
	{
		if (formftp.ftp_port.value == "")
		{
			alert('Please specify FTP Port');
			formftp.ftp_port.focus();
			formftp.ftp_port.select();
			return false;
		}
		else if (isNaN(form.ftp_port.value) ||
			 parseInt(formftp.ftp_port.value,10) > 65535 || (parseInt(formftp.ftp_port.value,10) < 1024 &&
			 parseInt(formftp.ftp_port.value,10) != 21))

		{
			alert('Please specify valid number');
			formftp.ftp_port.focus();
			formftp.ftp_port.select();
			return false;
		}
		
		if (isNaN(formftp.ftp_idle_timeout.value))
		{
			alert('Please specify valid number');
			formftp.ftp_idle_timeout.focus();
			formftp.ftp_idle_timeout.select();
			return false;
		}
	}

	return true;
}
</script>
</head>

<body onLoad="initValue();" >
<table class="body">
  <tr>
    <td><h1 id="ftpTitle">FTP Settings </h1>
      <p id="ftpIntroduction">Here you can configure the most basic settings of FTP server,
        such as FTP port and access to your FTP server.</p>
      <hr />
      <form method=post name=storageftp action="/goform/storageFtpSrv" onSubmit="return CheckValue()">
        <table class="form">
          <tr>
            <td class="title" colspan="2" id="ftpSrvSet">FTP Server Setup</td>
          </tr>
          <tr>
            <td class="head" id="ftpSrv">Enable FTP server</td>
            <td><select name="ftp_enabled" class="half" onChange="FtpEnableSwitch(this.form);" >
                <option value="0">Disable</option>
                <option value="1">Enable</option>
              </select></td>
          </tr>
          <tr>
            <td class="head" id="ftpSrv">FTP server access</td>
            <td><select name="ftp_access" class="half">
                <option value="0">LAN</option>
                <option value="1">LAN &amp; WAN</option>
              </select></td>
          </tr>
          <tr>
            <td class="head" id="ftpSrvPort">Access port</td>
            <td><input type=text name=ftp_port size=5 maxlength=5 value="<% getCfgGeneral(1, "FtpPort"); %>"></td>
          </tr>
          <tr>
            <td class="head" id="ftpSrvRootDir">Root directory</td>
            <td><input type=text name=ftp_rootdir size=50 maxlength=50 value="<% getCfgGeneral(1, "FtpRootDir"); %>"></td>
          </tr>
          <tr>
            <td class="head" id="ftpSrvLoginTimeout">Server idle timeout</td>
            <td><input type=text name=ftp_idle_timeout size=4 maxlength=4 value="<% getCfgGeneral(1, "FtpIdleTime"); %>"></td>
          </tr>
        </table>
        <table class="buttons">
          <tr>
            <td><input value="/usb/STORAGEftpsrv.asp" name="submit-url" type="hidden">
              <input type=submit class="normal" value="Apply" id="ftpApply" onClick="return CheckValue(this.form);" >
              &nbsp; &nbsp;
              <input type=reset  class="normal" value="Reset" id="ftpReset" onClick="window.location.reload()"></td>
          </tr>
        </table>
      </form>
      <br>
      <h1 id="dlnaTitle">MiniDLNA Server Settings </h1>
      <p id="ftpIntroduction">Here you can configure the most basic settings of MiniDLNA server.</p>
      <hr />
      <form method=post name=storagedlna action="/goform/storageDLNASrv">
        <input type=hidden name="hiddenButton" value="">
        <table class="form">
          <tr>
            <td class="title" colspan="2" id="smbShareDirList">DLNA settings</td>
          </tr>
          <tr>
            <td class="head" id="DlnaSrvEnable">Enable MiniDLNA Server</td>
            <td><select name="dlna_enabled" class="half" onChange="DlnaEnableSwitch(this.form);" >
                <option value="0">Disable</option>
                <option value="1">Enable</option>
              </select>
              &nbsp; &nbsp;
              <input type="button" style="{width:80px;}" value="Restart" name="dlnarestart" onClick="submit_apply('restart')"></td>
          </tr>
          <tr>
            <td class="head" id="DBUpdatemode">Database update mode</td>
            <td><select name="DBUpdate" class="mid">
                <option value="0">No update</option>
                <option value="1">Update added new objects</option>
                <option value="2">Full recreate database</option>
              </select>
              &nbsp; &nbsp;
              <input type="button" style="{width:80px;}" onClick="location.href='http://<% getLanIp(); %>:8200'" name="Status" value="Status"></td>
          </tr>
        </table>
        <table class="buttons">
          <tr>
            <td><input value="/usb/STORAGEftpsrv.asp" name="submit-url" type="hidden">
              <input type=submit class="normal" value="Apply" id="DLNAApply" onClick="submit_apply('apply')">
              &nbsp; &nbsp;
              <input type=reset  class="normal" value="Reset" id="DLNAReset" onClick="window.location.reload()"></td>
          </tr>
        </table>
      </form></td>
  </tr>
</table>
</body>
</html>
