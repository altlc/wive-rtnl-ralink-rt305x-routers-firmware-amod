<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate, post-check=0, pre-check=0">
<meta http-equiv="Pragma" content="no-cache">
<script type="text/javascript" src="/lang/b28n.js"></script>
<link rel="stylesheet" href="/style/normal_ws.css" type="text/css">
<link rel="stylesheet" href="/style/controls.css" type="text/css">
<title>Print Server Settings</title>
<script language="JavaScript" type="text/javascript">

function initValue(form)
{	
	form.enabled.value = '<% getCfgGeneral(1, "PrinterSrvEnabled"); %>';
	form.bdenabled.value = '<% getCfgGeneral(1, "PrinterSrvBidir"); %>';
}
</script>
</head>

<body onLoad="initValue(document.printer)">
<table class="body">
  <tr>
    <td><h1 id="printerTitle">Printer Server Settings</h1>
      <p id="printerIntroduction">Here you can configure printer server settings.
        If you printer need firmware - upload firmware in /etc/prnfw.dl on rwfs or /opt/prnfw.dl on optware dir in external drive.</p>
      <hr>
      <form method=POST name=printer action="/goform/printersrv">
        <table class="form">
          <tr>
            <td id="printerSettings" class="title" colspan="2">Printer Server Setup</td>
          </tr>
          <tr>
            <td id="printerCapability" class="head">Print Server</td>
            <td><select name="enabled" class="half">
                <option value="0">Disable</option>
                <option value="1">Enable</option>
              </select></td>
          </tr>
          <tr>
            <td class="head" id="printerBidirectional">Bidirectional exchange</td>
            <td><select name="bdenabled" class="half">
                <option value="0">Disable</option>
                <option value="1">Enable</option>
              </select></td>
          </tr>
        </table>
        <table class="buttons">
          <tr>
            <td><input type=submit class="normal" value="Apply" id="printerApply">
              &nbsp; &nbsp;
              <input type=button class="normal" value="Cancel" id="printerCancel" onClick="window.location.reload()">
              <input type="hidden" name="submit-url" value="/usb/P910NDprintersrv.asp"></td>
          </tr>
        </table>
      </form></td>
  </tr>
</table>
</body>
</html>
