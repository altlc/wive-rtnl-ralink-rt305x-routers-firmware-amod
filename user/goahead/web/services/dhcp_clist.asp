<table class="form">
  <tr>
    <td class="title" colspan="4" id="dClients">DHCP Clients</td>
  </tr>
  <tr style="background-color: #E8F8FF">
    <th>Hostname</th>
    <th>MAC Address</th>
    <th>IP Address</th>
    <th>Static</th>
  </tr>
  <% getDhcpCliList(); %>
</table>
