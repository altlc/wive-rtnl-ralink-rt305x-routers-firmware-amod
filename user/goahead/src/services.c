/* vi: set sw=4 ts=4 sts=4 fdm=marker: */
/*
 *	services.c -- services
 *
 *	Copyright (c) Ralink Technology Corporation All Rights Reserved.
 *
 *	$Id: services.c,v 1.167.2.8 2009-04-22 01:31:35 chhung Exp $
 */

#include	<stdlib.h>
#include	<sys/ioctl.h>
#include	<net/if.h>
#include	<net/route.h>
#include	<string.h>
#include	<dirent.h>

#include	"utils.h"
#include	"internet.h"
#include	"management.h"
#include	"station.h"
#include	"firewall.h"
#include	"helpers.h"
#include	"procps.h"

static void setDhcp(webs_t wp, char_t *path, char_t *query);
static int getDhcpCliList(int eid, webs_t wp, int argc, char_t **argv);
#ifdef CONFIG_USER_SAMBA
static void setSamba(webs_t wp, char_t *path, char_t *query);
#endif
static void setMiscServices(webs_t wp, char_t *path, char_t *query);
static void formIptAccounting(webs_t wp, char_t *path, char_t *query);
static int getDhcpStaticList(int eid, webs_t wp, int argc, char_t **argv);
static int iptStatList(int eid, webs_t wp, int argc, char_t **argv);

static void l2tpConfig(webs_t wp, char_t *path, char_t *query);
static int getL2TPUserList(int eid, webs_t wp, int argc, char_t **argv);
static int getARPptBuilt(int eid, webs_t wp, int argc, char_t **argv);
static int getTelnetdBuilt(int eid, webs_t wp, int argc, char_t **argv);
static int getSNMPDBuilt(int eid, webs_t wp, int argc, char_t **argv);
static int getProcessList(int eid, webs_t wp, int argc, char_t **argv);

void formDefineServices(void)
{
	// Define forms
	websFormDefine(T("setDhcp"), setDhcp);
#ifdef CONFIG_USER_SAMBA
	websFormDefine(T("formSamba"), setSamba);
#endif
	websFormDefine(T("setMiscServices"), setMiscServices);
	websFormDefine(T("formIptAccounting"), formIptAccounting);
	websFormDefine(T("l2tpConfig"), l2tpConfig);

	// Define functions
	websAspDefine(T("getL2TPUserList"), getL2TPUserList);
	websAspDefine(T("getDhcpCliList"), getDhcpCliList);
	websAspDefine(T("getDhcpStaticList"), getDhcpStaticList);
	websAspDefine(T("iptStatList"), iptStatList);
	websAspDefine(T("getARPptBuilt"), getARPptBuilt);
	websAspDefine(T("getTelnetdBuilt"), getTelnetdBuilt);
	websAspDefine(T("getSNMPDBuilt"), getSNMPDBuilt);
	websAspDefine(T("getProcessList"), getProcessList);
}

static int getProcessList(int eid, webs_t wp, int argc, char_t **argv)
{
	if (argc <= 0)
		return 0;

	cmdline_t *proc_list = procps_list();
	cmdline_t *curr = proc_list;
	
	while (curr != NULL)
	{
		int i=0;
		while (i < argc)
		{
			// Check if name matches
			if ((curr->argc > 0) && (strstr(curr->argv[0], argv[i]) != NULL))
			{
				websWrite(wp, "%s,", curr->argv[0]);
				break;
			}
			i++;
		}
		
		// Move pointer
		curr = curr->next;
	}

	// Free structure, NULL is properly handled
	procps_free(proc_list);
	
	return 0;
}

/*
 * description: write DHCP client list
 */
static int getDhcpCliList(int eid, webs_t wp, int argc, char_t **argv)
{
	FILE *fp;
	char buff[256], dh_lease[32], dh_mac[32], dh_ip[32], dh_host[64], dh_sid[32];

	int i, rownum = 0;

	//if DHCP is disabled - just exit
	char* dhcpEnabled = nvram_get(RT2860_NVRAM, "dhcpEnabled");
	if (!strncmp(dhcpEnabled, "0", 2))
		return 0;

	fp = fopen("/var/dnsmasq.leases", "r");
	if (!fp)
		return websWrite(wp, T(""));

	buff[0] = 0;
	while (fgets(buff, sizeof(buff), fp))
	{
		dh_lease[0] = 0;
		dh_mac[0] = 0;
		dh_ip[0] = 0;
		dh_host[0] = 0;
		dh_sid[0] = 0;
		if (sscanf(buff, "%s %s %s %s %s", dh_lease, dh_mac, dh_ip, dh_host, dh_sid) != 5)
			continue;
		
		strcat(dh_lease, " secs");
		
		if (!dh_host[0])
			strcpy(dh_host, "*");
		
		// convert MAC to upper case
		for (i=0; i<strlen(dh_mac); i++)
			dh_mac[i] = toupper(dh_mac[i]);
		
		websWrite(wp, T("<tr><td>%s&nbsp;</td>"), dh_host);
		websWrite(wp, T("<td id=\"dhclient_row%d_mac\">%s</td>"), rownum, (dh_mac[0]!=0) ? dh_mac : " " );
		websWrite(wp, T("<td id=\"dhclient_row%d_ip\">%s</td>"), rownum, (dh_ip[0]!=0) ? dh_ip : " ");
		websWrite(wp, "<td id=\"dhclient_row%d_status\" style=\"text-align: center;\">"
			"<input id=\"dhclient_row%d\" type=\"checkbox\" onchange=\"toggleDhcpTable(this);\"></td>",
			rownum, rownum);
		websWrite(wp, "</tr>\n");
		
		buff[0] = 0;
		rownum++;
	}
	fclose(fp);
	return 0;
}

static int getDhcpStaticList(int eid, webs_t wp, int argc, char_t **argv)
{
	// Get values
	FILE *fd = fopen(_PATH_DHCP_ALIAS_FILE, "r");

	if (fd != NULL)
	{
		char line[256];
		char ip[64], mac[64];
		int args, first = 1;

		while (fgets(line, 255, fd) != NULL)
		{
			// Read routing line
			args = sscanf(line, "%s %s", ip, mac);

			if (args >= 2)
			{
				if (!first)
					websWrite(wp, T(",\n"));
				else
					first = 0;

				websWrite(wp, T("\t[ '%s', '%s' ]"), ip, mac );
			}
		}

		//close file
		fclose(fd);
	}

	websWrite(wp, T("\n"));

	return 0;
}

void dhcpStoreAliases(const char *dhcp_config)
{
	// Open file
	FILE *fd = fopen(_PATH_DHCP_ALIAS_FILE, "w+");

	if (fd != NULL)
	{
		// Output routing table to file
		fwrite(dhcp_config, strlen(dhcp_config), 1, fd);
		fclose(fd);
		sync();

		// Call rwfs to store data
		system("fs save &");
	}
	else
		printf("Failed to open file %s\n", _PATH_DHCP_ALIAS_FILE);
}

const parameter_fetch_t dhcp_args[] =
{
	{ T("dhcpStart"),               "dhcpStart",            0,       T("") },
	{ T("dhcpEnd"),                 "dhcpEnd",              0,       T("") },
	{ T("dhcpMask"),                "dhcpMask",             0,       T("") },
	{ T("dhcpGateway"),             "dhcpGateway",          0,       T("") },
	{ T("dhcpLease"),               "dhcpLease",            0,       T("86400") },
	{ T("dhcpDomain"),              "dhcpDomain",           0,       T("localdomain") },
	{ NULL, NULL, 0, NULL }
};

const parameter_fetch_t dhcp_args_dns[] =
{
	{ T("dhcpPriDns"),              "dhcpPriDns",           0,       T("") },
	{ T("dhcpSecDns"),              "dhcpSecDns",           0,       T("") },
	{ NULL, NULL, 0, NULL }
};

/* goform/setDhcp */
static void setDhcp(webs_t wp, char_t *path, char_t *query)
{
	char_t	*dhcp_tp, *static_leases;

	dhcp_tp = websGetVar(wp, T("lanDhcpType"), T("DISABLE"));
	static_leases = websGetVar(wp, T("dhcpAssignIP"), T(""));

	// configure gateway and dns (WAN) at bridge mode
	if (strncmp(dhcp_tp, "SERVER", 7)==0)
	{
		nvram_init(RT2860_NVRAM);
		nvram_bufset(RT2860_NVRAM, "dhcpEnabled", "1");
		setupParameters(wp, dhcp_args, 0);

		char *dns_proxy = nvram_bufget(RT2860_NVRAM, "dnsPEnabled");
		if (CHK_IF_DIGIT(dns_proxy, 1))
		{
			nvram_bufset(RT2860_NVRAM, "dhcpPriDns", "");
			nvram_bufset(RT2860_NVRAM, "dhcpSecDns", "");
		}
		else
			setupParameters(wp, dhcp_args_dns, 0);

		nvram_commit(RT2860_NVRAM);
		nvram_close(RT2860_NVRAM);
	}
	else if (strncmp(dhcp_tp, "DISABLE", 8)==0)
		nvram_set(RT2860_NVRAM, "dhcpEnabled", "0");

	// Restart DHCP service
	doSystem("service dhcpd restart");

	// Store leases to rwfs. ALWAYS AFTER RESTART DHCP!!!
	if (strncmp(dhcp_tp, "SERVER", 7)==0)
	{
		Sleep(2);
		dhcpStoreAliases(static_leases);
	}

	char_t *submitUrl = websGetVar(wp, T("submit-url"), T(""));   // hidden page
	if (submitUrl != NULL)
		websRedirect(wp, submitUrl);
	else
		websDone(wp, 200);
}

static int getSNMPDBuilt(int eid, webs_t wp, int argc, char_t **argv)
{
#ifdef CONFIG_USER_SNMPD
       websWrite(wp, T("1"));
#else
       websWrite(wp, T("0"));
#endif
       return 0;
}

static int getARPptBuilt(int eid, webs_t wp, int argc, char_t **argv)
{
#ifdef CONFIG_USER_PARPROUTED
       websWrite(wp, T("1"));
#else
       websWrite(wp, T("0"));
#endif
       return 0;
}

static int getTelnetdBuilt(int eid, webs_t wp, int argc, char_t **argv)
{
#ifdef CONFIG_TELNETD
	websWrite(wp, T("1"));
#else
	websWrite(wp, T("0"));
#endif
	return 0;
}

const parameter_fetch_t service_misc_flags[] =
{
	{ T("stpEnbl"), "stpEnabled", 0, T("0") },
#ifdef CONFIG_USER_CDP
	{ T("cdpEnbl"), "cdpEnabled", 0, T("0") },
#endif
	{ T("lltdEnbl"), "lltdEnabled", 0, T("0") },
	{ T("igmpEnbl"), "igmpEnabled", 0, T("0") },
	{ T("igmpSnoop"), "igmpSnoopMode", 0, T("") },
	{ T("upnpEnbl"), "upnpEnabled", 0, T("0") },
	{ T("xupnpdEnbl"), "xupnpd", 0, T("0") },
	{ T("radvdEnbl"), "radvdEnabled", 0, T("0") },
	{ T("pppoeREnbl"), "pppoeREnabled", 0, T("0") },
	{ T("dnspEnbl"), "dnsPEnabled", 0, T("0") },
	{ T("rmtHTTP"), "RemoteManagement", 0, T("0") },
	{ T("RemoteManagementPort"), "RemoteManagementPort", 0, T("80") },
	{ T("rmtSSH"), "RemoteSSH", 0, T("0") },
#ifdef CONFIG_TELNETD
	{ T("rmtTelnet"), "RemoteTelnet", 0, T("0") },
#endif
	{ T("udpxyMode"), "UDPXYMode", 0, T("0") },
	{ T("udpxyPort"), "UDPXYPort", 0, T("81") },
	{ T("watchdogEnable"), "WatchdogEnabled", 0, T("0") },
	{ T("pingWANEnbl"), "WANPingFilter", 0, T("0") },
	{ T("krnlPppoePass"), "pppoe_pass", 0, T("0") },
	{ T("krnlIpv6Pass"), "ipv6_pass", 0, T("0") },
	{ T("dhcpSwReset"), "dhcpSwReset", 0, T("0") },
	{ T("natFastpath"), "natFastpath", 0, T("0") },
	{ T("hw_nat_wifiPT"), "hw_nat_wifi", 0, T("0") },
	{ T("ppeUDP"), "hw_nat_UDP", 0, T("0") }, 
	{ T("natMode"), "nat_mode", 0, T("1") },
	{ T("bridgeFastpath"), "bridgeFastpath", 0, T("1") },
	{ T("CrondEnable"), "CrondEnable", 0, T("0") },
	{ T("ForceRenewDHCP"), "ForceRenewDHCP", 0, T("1") },
#ifdef CONFIG_USER_PARPROUTED
	{ T("arpPT"), "parproutedEnabled", 0, T("0") },
#endif
	{ T("pingerEnable"), "pinger_check_on", 0, T("0") },
	{ T("ping_check_time"), "ping_check_time", 0, T("0") },
	{ T("ping_check_interval"), "ping_check_interval", 0, T("0") },
	{ T("ttlStore"), "store_ttl", 0, T("0") },
	{ T("ttlMcastStore"), "store_ttl_mcast", 0, T("0") },
#ifdef CONFIG_USER_SNMPD
	{ T("SnmpdEnabled"), "snmpd", 0, T("0") },
#endif
	{ T("mssPmtu"), "mss_use_pmtu", 0, T("1") },
	{ NULL, NULL, 0, NULL } // Terminator
};

/* goform/setMiscServices */
static void setMiscServices(webs_t wp, char_t *path, char_t *query)
{
	nvram_init(RT2860_NVRAM);

	setupParameters(wp, service_misc_flags, 0);

	char_t *nat_fp = nvram_bufget(RT2860_NVRAM, "natFastpath");
	if (CHK_IF_DIGIT(nat_fp, 2) || CHK_IF_DIGIT(nat_fp, 3))
	{
		char_t *nat_th = websGetVar(wp, "hwnatThreshold", "30");
		if (nat_th != NULL)
			nvram_bufset(RT2860_NVRAM, "hw_nat_bind", nat_th);
	}

	char_t *dns_proxy = nvram_bufget(RT2860_NVRAM, "dnsPEnabled");
	if (CHK_IF_DIGIT(dns_proxy, 1))
	{
		nvram_bufset(RT2860_NVRAM, "dhcpPriDns", "");
		nvram_bufset(RT2860_NVRAM, "dhcpSecDns", "");
	}
	
	char_t *http_port = nvram_bufget(RT2860_NVRAM, "RemoteManagementPort");

	nvram_close(RT2860_NVRAM);

	char_t *port_changed = websGetVar(wp, T("rmt_http_port_changed"), T("0"));
	
	if (CHK_IF_DIGIT(port_changed, 1))
	{
		char lan_if_addr[32];
		const char *lan_if_ip;

		// Get IP
		if (getIfIp(getLanIfName(), lan_if_addr) != -1)
			lan_if_ip = lan_if_addr;
		else
		{
			lan_if_ip = nvram_get(RT2860_NVRAM, "lan_ipaddr");
			if (lan_if_ip == NULL)
				lan_if_ip = "192.168.1.1";
		}

		// Output reloading
		websHeader(wp);
		if (strcmp(http_port, "80") == 0)
		{
			websWrite
			(
				wp,
				T(
				"<script type=\"text/javascript\" src=\"/js/ajax.js\"></script>\n"
				"<script language=\"JavaScript\" type=\"text/javascript\">\n"
				"ajaxReloadDelayedPage(%ld, \"http://%s\");\n"
				"</script>"),
				50000, lan_if_ip
			);
		}
		else
		{
			websWrite
			(
				wp,
				T(
				"<script type=\"text/javascript\" src=\"/js/ajax.js\"></script>\n"
				"<script language=\"JavaScript\" type=\"text/javascript\">\n"
				"ajaxReloadDelayedPage(%ld, \"http://%s:%s\");\n"
				"</script>"),
				50000, lan_if_ip, http_port
			);
		}
		websFooter(wp);
		websDone(wp, 200);
		
		doSystem("sleep 3 && reboot");
	}
	else
	{
		//restart some services instead full reload
		doSystem("services_restart.sh misc");

		char_t *submitUrl = websGetVar(wp, T("submit-url"), T(""));   // hidden page
		if (submitUrl != NULL)
			websRedirect(wp, submitUrl);
		else
			websDone(wp, 200);
	}
}

#ifdef CONFIG_USER_SAMBA
//------------------------------------------------------------------------------
// Samba/CIFS setup
const parameter_fetch_t service_samba_flags[] =
{
	{ T("WorkGroup"), "WorkGroup", 0, T("") },
	{ T("SmbNetBIOS"), "SmbNetBIOS", 0, T("") },
	{ T("SmbString"), "SmbString", 0, T("") },
	{ T("SmbOsLevel"), "SmbOsLevel", 0, T("") },
	{ T("SmbTimeserver"), "SmbTimeserver", 0, T("0") },
	{ NULL, NULL, 0, NULL } // Terminator
};

static void setSamba(webs_t wp, char_t *path, char_t *query)
{
	char_t *smb_enabled = websGetVar(wp, T("SmbEnabled"), T("0"));
	if (smb_enabled == NULL)
		smb_enabled = "0";

	nvram_init(RT2860_NVRAM);
	nvram_bufset(RT2860_NVRAM, "SmbEnabled", smb_enabled);

	if (CHK_IF_DIGIT(smb_enabled, 1))
		setupParameters(wp, service_samba_flags, 0);

	nvram_close(RT2860_NVRAM);

	//restart some services instead full reload
	doSystem("service sysctl restart");
	doSystem("service dhcpd restart");
	doSystem("service iptables restart");
	doSystem("service samba restart");

	char_t *submitUrl = websGetVar(wp, T("submit-url"), T(""));   // hidden page
	if (submitUrl != NULL)
		websRedirect(wp, submitUrl);
	else
		websDone(wp, 200);
}
#endif
//------------------------------------------------------------------------------
// IPT Accounting
void formIptAccounting(webs_t wp, char_t *path, char_t *query)
{
	char_t *strValue;
	int reset_ipt = 0;

	strValue = websGetVar(wp, T("iptEnable"), T("0"));	//reset stats
	if ((strValue != NULL) && (strcmp(strValue, "0")==0))
		reset_ipt = 1;

	nvram_set(RT2860_NVRAM, "ipt_account", strValue);

	strValue = websGetVar(wp, T("reset"), T("0"));	//reset stats
	if ((strValue != NULL) && (strcmp(strValue, "1")))
		reset_ipt = 1;

	// Reset IPT
	if (reset_ipt)
	{
		FILE *fd = fopen(_PATH_IPT_ACCOUNTING_FILE, "w");
		if (fd != NULL)
		{
			fputs("reset", fd);
			fclose(fd);
		}
	}

	doSystem("modprobe -q ipt_account");
	doSystem("service iptables restart");

	char_t *submitUrl = websGetVar(wp, T("submit-url"), T(""));   // hidden page
	if (submitUrl != NULL)
		websRedirect(wp, submitUrl);
	else
		websDone(wp, 200);
}

#ifndef IPT_SHORT_ACCOUNT
const char *iptProtocolNames[]=
{
	"All", "TCP", "UDP", "ICMP", "Others"
};
#endif /* !IPT_SHORT_ACCOUNT */

// Output IP Accounting statistics
int iptStatList(int eid, webs_t wp, int argc, char_t **argv)
{
	FILE *fd;
	char_t ip[32], line[256];
	long long b_src[5], p_src[5], b_dst[5], p_dst[5], time;
	int lines = 0;

	// Do not show anything if nat_fastpath is set
	char* nat_fp = nvram_get(RT2860_NVRAM, "natFastpath");
	if (nat_fp != NULL)
	{
		if (strcmp(nat_fp, "0") != 0)
		{
			websWrite(wp, T("<tr><td>No IPT accounting allowed</td></tr>\n"));
			return 0;
		}
	}

	websWrite(wp, T("<tr><td class=\"title\" colspan=\"%d\">IP Accounting table</td></tr>\n"),
#ifdef IPT_SHORT_ACCOUNT
				6
#else
				7
#endif
	);

	if ((fd = fopen(_PATH_IPT_ACCOUNTING_FILE, "r"))!=NULL)
	{
		// Output table header
#ifdef IPT_SHORT_ACCOUNT
		websWrite(wp, T(
			"<tr>\n"
			"<th width=\"30%%\" align=\"center\">IP addr</th>\n"
			"<th width=\"15%%\" align=\"center\">Tx bts</th>\n"
			"<th width=\"15%%\" align=\"center\">Tx pkt</th>\n"
			"<th width=\"15%%\" align=\"center\">Rx bts</th>\n"
			"<th width=\"15%%\" align=\"center\">Rx pkt</th>\n"
			"<th width=\"10%%\" align=\"center\">Time</th>\n"
			"</tr>\n"));
#else
		websWrite(wp, T(
			"<tr>\n"
			"<th width=\"30%%\" align=\"center\">IP addr</th>\n"
			"<th width=\"10%%\" align=\"center\">Proto</th>\n"
			"<th width=\"13%%\" align=\"center\">Tx bts</th>\n"
			"<th width=\"13%%\" align=\"center\">Tx pkt</th>\n"
			"<th width=\"13%%\" align=\"center\">Rx bts</th>\n"
			"<th width=\"13%%\" align=\"center\">Rx pkt</th>\n"
			"<th width=\"8%%\" align=\"center\">Time</th>\n"
			"</tr>\n"));

#endif /* IPT_SHORT_ACCOUNT */

		while (fgets(line, 255, fd)!=NULL)
		{
			lines++;
#ifdef IPT_SHORT_ACCOUNT
			sscanf(line,
				"%*s %*s %s "   // IP
				"%*s %*s %lld "  // bytes_src
				"%*s %*s %lld "  // packets_src
				"%*s %*s %lld "  // bytes_dst
				"%*s %*s %lld "  // packets_dst
				"%*s %*s %lld ", // time
				ip, &b_src[0], &p_src[0], &b_dst[0], &p_dst[0], &time);

			websWrite(wp, T(
				"<tr class=\"grey\">\n"
				"<td width=\"30%%\" align=\"center\">%s</td>\n"),
				ip);

			const char *src_sz = normalizeSize(&b_src[0]);
			const char *dst_sz = normalizeSize(&b_dst[0]);

			websWrite(wp, T(
				"<td width=\"15%%\" align=\"center\">%ld %s</td>\n"
				"<td width=\"15%%\" align=\"center\">%ld</td>\n"
				"<td width=\"15%%\" align=\"center\">%ld %s</td>\n"
				"<td width=\"15%%\" align=\"center\">%ld</td>\n"
				),
				(long)b_src[0], src_sz, (long)p_src[0], (long)b_dst[0], dst_sz, (long)p_dst[0]
				);

			websWrite(wp, T(
				"<td width=\"10%%\" align=\"center\">%ld</td>\n"
				"</tr>\n"),
				(long)time);
#else
			sscanf(line,
				"%*s %*s %s "   // IP
				"%*s %*s %lld %lld %lld %lld %lld "  // bytes_src
				"%*s %*s %lld %lld %lld %lld %lld "  // packets_src
				"%*s %*s %lld %lld %lld %lld %lld "  // bytes_dst
				"%*s %*s %lld %lld %lld %lld %lld "  // packets_dst
				"%*s %*s %lld ", // time
				ip,
				&b_src[0], &b_src[1], &b_src[2], &b_src[3], &b_src[4],
				&p_src[0], &p_src[1], &p_src[2], &p_src[3], &p_src[4],
				&b_dst[0], &b_dst[1], &b_dst[2], &b_dst[3], &b_dst[4],
				&p_dst[0], &p_dst[1], &p_dst[2], &p_dst[3], &p_dst[4],
				&time);

			websWrite(wp, T(
				"<tr class=\"grey\">\n"
				"<td width=\"30%%\" align=\"center\">%s</td>\n"),
				ip);

			int i;
			for (i=0; i<5; i++)
			{
				if (i>0)
					websWrite(wp, T("<tr class=\"grey\">\n"));

				const char *src_sz = normalizeSize(&b_src[i]);
				const char *dst_sz = normalizeSize(&b_dst[i]);
				websWrite(wp, T(
					"<td width=\"10%%\" align=\"center\">%s</font></td>\n"
					"<td width=\"13%%\" align=\"center\">%lld %s</td>\n"
					"<td width=\"13%%\" align=\"center\">%lld</td>\n"
					"<td width=\"13%%\" align=\"center\">%lld %s</td>\n"
					"<td width=\"13%%\" align=\"center\">%lld</td>\n"),
					iptProtocolNames[i], b_src[i], src_sz, p_src[i], b_dst[i], dst_sz, p_dst[i]);

				if (i==0)
				{
					websWrite(wp, T(
						"<td width=\"10%%\" align=\"center\">%lld</td>\n"),
						time);
				}
				websWrite(wp, T("</tr>\n"));
			}
#endif /* IPT_SHORT_ACCOUNT */
		}

		fclose(fd);
	}

	if (lines<=0)
		websWrite(wp, T("<tr><td align=\"left\" colspan=\"%d\">No statistics available now</td></tr>\n"),
#ifdef IPT_SHORT_ACCOUNT
				6
#else
				7
#endif
			);
	return 0;
}

const parameter_fetch_t service_l2tp_flags[] =
{
	{ T("l2tp_srv_ip_range"), "l2tp_srv_ip_range", 0, T("") },
	{ T("l2tp_srv_ip_local"), "l2tp_srv_ip_local", 0, T("") },
	{ T("l2tp_srv_lcp_adapt"), "l2tp_srv_lcp_adapt", 2, T("off") },
	{ T("l2tp_srv_debug"), "l2tp_srv_debug", 2, T("off") },
	{ T("l2tp_srv_nat_enabled"), "l2tp_srv_nat_enabled", 2, T("off") },
	{ T("l2tp_srv_mtu_size"), "l2tp_srv_mtu_size", 0, T("1500") },
	{ T("l2tp_srv_mru_size"), "l2tp_srv_mru_size", 0, T("1500") },
	{ NULL, NULL, 0, NULL } // Terminator
};

static void l2tpConfig(webs_t wp, char_t *path, char_t *query)
{
	char user_var[16] = "l2tp_srv_user0";
	char pass_var[16] = "l2tp_srv_pass0";
	int i=0;

	nvram_init(RT2860_NVRAM);

	char_t *l2tp_enabled = websGetVar(wp, T("l2tp_srv_enabled"), T("0"));
	if (l2tp_enabled == NULL)
		l2tp_enabled = "0";

	nvram_bufset(RT2860_NVRAM, "l2tp_srv_enabled", l2tp_enabled);
	if (CHK_IF_DIGIT(l2tp_enabled,1))
	{
		setupParameters(wp, service_l2tp_flags, 0);

		// Set-up logins
		for (; i < 10; i++)
		{
			char_t *user = websGetVar(wp, user_var, "");
			char_t *pass = websGetVar(wp, pass_var, "");

			if (!(CHK_IF_SET(user) || CHK_IF_SET(pass)))
			{
				user = "";
				pass = "";
			}

			nvram_bufset(RT2860_NVRAM, user_var, user);
			nvram_bufset(RT2860_NVRAM, pass_var, pass);

			user_var[13]++;
			pass_var[13]++;
		}
	}
	
	nvram_commit(RT2860_NVRAM);
	nvram_close(RT2860_NVRAM);

	doSystem("service vpnserver restart");
	doSystem("service iptables restart");

	// Redirect if possible
	char_t *submitUrl = websGetVar(wp, T("submit-url"), T(""));   // hidden page
	if (submitUrl != NULL)
		websRedirect(wp, submitUrl);
	else
		websDone(wp, 200);
}

static int getL2TPUserList(int eid, webs_t wp, int argc, char_t **argv)
{
	//                   01234567890123
	char user_var[16] = "l2tp_srv_user0";
	char pass_var[16] = "l2tp_srv_pass0";
	int i = 0, output = 0;

	nvram_init(RT2860_NVRAM);
	for (; i < 10; i++)
	{
		char *user = nvram_bufget(RT2860_NVRAM, user_var);
		char *pass = nvram_bufget(RT2860_NVRAM, pass_var);

		if (CHK_IF_SET(user) || CHK_IF_SET(pass))
			websWrite(wp, T("%s[ '%s', '%s' ]"), ((output++) > 0) ? ",\n\t" : "\t", user, pass);
		user_var[13]++;
		pass_var[13]++;
	}
	nvram_close(RT2860_NVRAM);

	return 0;
}

