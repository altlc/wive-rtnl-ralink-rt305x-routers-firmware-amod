
///////////////UPLOAD OPTIONS/////////////////
#define RFC_ERROR 		"RFC1867 ...."
#define REFRESH_TIMEOUT         "80000"         /* 80000 = 80 secs*/
#define MEM_SIZE        	1024
#define MEM_HALF        	512

///////////////UPDATE OPTIONS/////////////////
#define MIN_SPACE_FOR_FIRMWARE  (1024 * 1024 * 8) // minimum space for firmware upload
#define MIN_FIRMWARE_SIZE       (1048576) 	  // minimum firmware size(1MB)

#define DEFAULT_LAN_IP 		"192.168.1.1"
