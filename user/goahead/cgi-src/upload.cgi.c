#include "upload.h"
#include "../options.h"

#ifdef UPLOAD_FIRMWARE_SUPPORT

static unsigned int getMTDPartSize(char *part)
{
	char buf[128], name[32], size[32], dev[32], erase[32];
	unsigned int result=0;
	FILE *fp = fopen("/proc/mtd", "r");
	if(!fp){
		fprintf(stderr, "mtd support not enable?");
		return 0;
	}
	while(fgets(buf, sizeof(buf), fp)){
		sscanf(buf, "%s %s %s %s", dev, size, erase, name);
		if(!strcmp(name, part)){
			result = strtol(size, NULL, 16);
			break;
		}
	}
	fclose(fp);
	return result;
}

static int mtd_write_firmware(char *filename, int offset, int len)
{
    char cmd[512];
    int status;
    int err=0;

/* check image size before erase flash and write image */
#ifdef CONFIG_RT2880_ROOTFS_IN_FLASH
    if(len > MAX_IMG_SIZE || len < MIN_FIRMWARE_SIZE){
	fprintf(stderr, "Image size is not correct!!!%d", len);
        return -1;
    }
#endif

#if defined(CONFIG_RT2880_FLASH_8M) || defined(CONFIG_RT2880_FLASH_16M)
#ifdef CONFIG_RT2880_FLASH_TEST
    /* workaround: erase 8k sector by myself instead of mtd_erase */
    /* this is for bottom 8M NOR flash only */
    snprintf(cmd, sizeof(cmd), "/bin/flash -f 0x400000 -l 0x40ffff");
    status = system(cmd);
    if (!WIFEXITED(status) || WEXITSTATUS(status) != 0)
	err++;
#endif
#endif
#if defined(CONFIG_RT2880_ROOTFS_IN_RAM)
    snprintf(cmd, sizeof(cmd), "/bin/mtd_write -o %d -l %d write %s Kernel", offset, len, filename);
    status = system(cmd);
    if (!WIFEXITED(status) || WEXITSTATUS(status) != 0)
	err++;
#elif defined(CONFIG_RT2880_ROOTFS_IN_FLASH)
  #ifdef CONFIG_ROOTFS_IN_FLASH_NO_PADDING
    snprintf(cmd, sizeof(cmd), "/bin/mtd_write -o %d -l %d write %s Kernel_RootFS", offset, len, filename);
    status = system(cmd);
    if (!WIFEXITED(status) || WEXITSTATUS(status) != 0)
	err++;
  #else
    snprintf(cmd, sizeof(cmd), "/bin/mtd_write -o %d -l %d write %s Kernel", offset,  CONFIG_MTD_KERNEL_PART_SIZ, filename);
    status = system(cmd);
    if (!WIFEXITED(status) || WEXITSTATUS(status) != 0)
	err++;

    snprintf(cmd, sizeof(cmd), "/bin/mtd_write -o %d -l %d write %s RootFS", offset + CONFIG_MTD_KERNEL_PART_SIZ, len - CONFIG_MTD_KERNEL_PART_SIZ, filename);
    status = system(cmd);
    if (!WIFEXITED(status) || WEXITSTATUS(status) != 0)
	err++;
  #endif
#else
    fprintf(stderr, "goahead: no CONFIG_RT2880_ROOTFS defined!");
#endif
    if (err == 0)
        return 0;
    else
        return -1;
}

/*
 *  taken from "mkimage -l" with few modified....
 */
int check(char *imagefile, int offset, int len, char *err_msg)
{
	struct stat sbuf;

	int  data_len;
	char *data;
	unsigned char *ptr;
	unsigned long checksum;

	image_header_t header;
	image_header_t *hdr = &header;

	int ifd;

	if ((unsigned)len < sizeof(image_header_t)) {
		sprintf (err_msg, "Bad size: \"%s\" is no valid image\n", imagefile);
		return 0;
	}

	ifd = open(imagefile, O_RDONLY);
	if(!ifd){
		sprintf (err_msg, "Can't open %s: %s\n", imagefile, strerror(errno));
		return 0;
	}

	if (fstat(ifd, &sbuf) < 0) {
		close(ifd);
		sprintf (err_msg, "Can't stat %s: %s\n", imagefile, strerror(errno));
		return 0;
	}

	ptr = (unsigned char *) mmap(0, sbuf.st_size, PROT_READ, MAP_SHARED, ifd, 0);
	if ((caddr_t)ptr == (caddr_t)-1) {
		close(ifd);
		sprintf (err_msg, "Can't mmap %s: %s\n", imagefile, strerror(errno));
		return 0;
	}
	ptr += offset;

	/*
	 *  handle Header CRC32
	 */
	memcpy (hdr, ptr, sizeof(image_header_t));

	if (ntohl(hdr->ih_magic) != IH_MAGIC)
	{
		munmap(ptr, len);
		close(ifd);
		sprintf (err_msg, "Bad Magic Number: \"%s\" is no valid image\n", imagefile);
		return 0;
	}

	data = (char *)hdr;

	checksum = ntohl(hdr->ih_hcrc);
	hdr->ih_hcrc = htonl(0);	/* clear for re-calculation */

	if (crc32 (0, data, sizeof(image_header_t)) != checksum)
	{
		munmap(ptr, len);
		close(ifd);
		sprintf (err_msg, "*** Warning: \"%s\" has bad header checksum!\n", imagefile);
		return 0;
	}

	/*
	 *  handle Data CRC32
	 */
	data = (char *)(ptr + sizeof(image_header_t));
	data_len  = len - sizeof(image_header_t) ;

	if (crc32 (0, data, data_len) != ntohl(hdr->ih_dcrc))
	{
		munmap(ptr, len);
		close(ifd);
		sprintf (err_msg, "*** Warning: \"%s\" has corrupted data!\n", imagefile);
		return 0;
	}

	/*
	 * compare MTD partition size and image size
	 */
#if defined(CONFIG_RT2880_ROOTFS_IN_RAM)
	if(len > MAX_IMG_SIZE || len > getMTDPartSize("\"Kernel\"")){
		munmap(ptr, len);
		close(ifd);
		sprintf(err_msg, "*** Error: the image file(0x%x) is bigger than Kernel MTD partition.\n", len);
		return 0;
	}
#elif defined(CONFIG_RT2880_ROOTFS_IN_FLASH)
  #ifdef CONFIG_ROOTFS_IN_FLASH_NO_PADDING
	if(len > MAX_IMG_SIZE || len > getMTDPartSize("\"Kernel_RootFS\"")){
		munmap(ptr, len);
		close(ifd);
		sprintf(err_msg, "*** Error: the image file(0x%x) is bigger than Kernel_RootFS MTD partition.\n", len);
		return 0;
	}
  #else
	if(len > MAX_IMG_SIZE || len < CONFIG_MTD_KERNEL_PART_SIZ){
		munmap(ptr, len);
		close(ifd);
		sprintf(err_msg, "*** Error: the image file(0x%x) size doesn't make sense.\n", len);
		return 0;
	}

	if((len - CONFIG_MTD_KERNEL_PART_SIZ) > getMTDPartSize("\"RootFS\"")){
		munmap(ptr, len);
		close(ifd);
		sprintf(err_msg, "*** Error: the image file(0x%x) is bigger than RootFS MTD partition.\n", len - CONFIG_MTD_KERNEL_PART_SIZ);
		return 0;
	}
  #endif
#else
#error "goahead: no CONFIG_RT2880_ROOTFS defined!"
#endif
	munmap(ptr, len);
	close(ifd);

	return 1;
}

int main (int argc, char *argv[])
{
	char err_msg[256];
	long file_size = 0;
	long file_begin = 0, file_end = 0;

	// Get multipart file data separator
	char separator[MAX_SEPARATOR_LEN];

	html_header();

	if (get_content_separator(separator, sizeof(separator), &file_size) < 0)
	{
		html_error(RFC_ERROR);
		return -1;
	}

	// Get multipart file name
	char *filename = getenv("UPLOAD_FILENAME");
	if (filename == NULL)
	{
		html_error(RFC_ERROR);
		return -1;
	}

	// Wait until file is completely uploaded
	int tries = 0;
	while (tries>5)
	{
		struct stat filestat;
		if (stat(filename, &filestat)>0)
		{
			if (filestat.st_size >= file_size) // Size ok?
				break;
		}

		sleep(1000);
		tries++;
	}

	// Open file
	FILE *fd = fopen(filename, "r");
	if (fd == NULL)
	{
		html_error(RFC_ERROR);
		return -1;
	}

	// Parse parameters
	parameter_t *params;
	if (read_parameters(fd, separator, &params)<0)
	{
		fclose(fd);
		html_error(RFC_ERROR);
		return -1;
	}

	fclose(fd);

	// Find parameter containing NVRAM reset flag
	parameter_t *find = find_parameter(params, "reset_rwfs");
	int reset_rwfs = 0;
	if (find != NULL)
	{
		if (find->value != NULL)
			reset_rwfs = (strcasecmp(find->value, "on")==0);
	}

	// Find parameter containing file
	find = find_parameter(params, "filename");
	if (find != NULL)
	{
		// Check if parameter is correct
		if (find->content_type == NULL)
		{
			html_error(RFC_ERROR);
			return -1;
		}
		if (!check_binary_content_type(find->content_type))
		{
			sprintf(err_msg, "Unsupported content-type for binary data: %s", find->content_type);
			html_error(err_msg);
			return -1;
		}

		file_begin = find->start_pos;
		file_end   = find->end_pos;
	}
	else
	{
		html_error("No firmware binary file");
		return -1;
	}

	release_parameters(params);

	// check image size
	if (!check(filename, (int)file_begin, (int)(file_end - file_begin), err_msg))
	{
		html_error("Not a valid firmware.");
		return -1;
	}

	// examination, reset NVRAM if needed
	// start web timer and crash rwfs BEFORE flash destroy
	if (reset_rwfs)
	{
		system("echo \"1234567890\" > /dev/mtdblock5");
		html_success(20*(IMAGE1_SIZE/0x100000) + 55);
	}
	else
		html_success(20*(IMAGE1_SIZE/0x100000) + 35);

	// Output success message
	fflush(stdout);
	fclose(stdout);

	// flash write
	if (mtd_write_firmware(filename, (int)file_begin, (file_end - file_begin)) == -1)
	{
		return -1;
	}

	//printf("Update complete. Reboot...\n");
	sleep (3);
	reboot(RB_AUTOBOOT);
	return 0;
}

#else
#error "no upload support defined!"
#endif
