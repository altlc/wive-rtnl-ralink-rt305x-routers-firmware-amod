#!/bin/sh

# Wive-NG  udhcpc script

# include global config
. /etc/scripts/global.sh

[ -z "$1" ] && echo "Error: should be called from udhcpc" && exit 1

LOG="logger -t udhcpc"
RESOLV_CONF="/etc/resolv.conf"
WINS_CONF="/tmp/wins.dhcp"

# Full route list
ROUTELIST=""
# Default gateway list
ROUTELIST_DGW=""
# Stub for first hop`s dgw
ROUTELIST_FGW=""

# Get MTU config and VPN DGW mode
eval `nvram_buf_get 2860 wan_manual_mtu vpnDGW dhcpSwReset lan_ipaddr lan_netmask`

# Renew flag
FULL_RENEW=1

# If pppoe mode and dgw in pppoe no need replace default gw
REPLACE_DGW=1
if [ "$vpnEnabled" = "on" -a "$vpnDGW" = "1" -a "$vpnType" = "0" ]; then
    REPLACE_DGW=0
fi

[ -n "$broadcast" ] && BROADCAST="broadcast $broadcast"
[ -n "$subnet" ] && NETMASK="netmask $subnet"

case "$1" in
    deconfig)
	# first stop vpn. prevent loop
	service vpnhelper stop_safe
	# generate random ip from zeroconfig range end set
	# this is hack for some ISPs checked client alive by arping
	# and prevent fake unset FULL_RENEW flag at next time bound
	rndip="169.254.$(($RANDOM%253+1)).$(($RANDOM%253+1))"
	ip addr flush dev $interface
	ip addr add $rndip/32 dev $interface
	# never need down iface
	ip link set $interface up
	rm -f $WINS_CONF
    ;;

    leasefail)
	service vpnhelper stop_safe
	# Workaround for infinite OFFER wait
	if [ "$OperationMode" != "2" -a "$dhcpSwReset" = "1" ]; then
	    if [ "$CONFIG_RT_3052_ESW" != "" ]; then
		# Reset switch to uplink touch
		$LOG "Restart WAN switch port."
		/etc/scripts/config-vlan.sh WWWWW
	    fi
	elif [ "$OperationMode" = "2" ]; then
	    # Try reconnect at lease failed
	    $LOG "Wait connect and reconnect to AP if need."
	    wait_connect reconnect
	fi
	rm -f $WINS_CONF
    ;;

    renew|bound)
	NEW_IP="$ip"
	OLD_IP=`ip -4 addr show dev $interface | awk '/inet / {print $2}' | cut -f1 -d"/"` > /dev/null 2>&1
	# set full renew flag
	if [ "$NEW_IP" = "$OLD_IP" ]; then
	    FULL_RENEW=0
	fi

    ########################################################################################################
    # IP/NETMASK/MTU
    ########################################################################################################

	# Small check
	if [ "$NEW_IP" = "" -o "$NETMASK" = "" ]; then
	    $LOG "ERROR: DHCP not send IP/MASK.... Call you provider support!!!"
	    exit 1
	fi

	# Check subnets
	lan_net=`ipcalc "$lan_ipaddr" "$lan_netmask" -ns | cut -f 2- -d =`
	wan_net=`ipcalc "$NEW_IP" "$NETMASK" -ns | cut -f 2- -d =`
	if [ "$NEW_IP" = "$lan_ipaddr" -o "$lan_net" = "$wan_net" ]; then
	    $LOG "ERROR: WAN ip in lan subnet. Need change LAN_IP address!!!"
	fi

	# MTU is default for all session time.
	if [ "$FULL_RENEW" = "1" ]; then
	    $LOG "Renew ip adress $NEW_IP and $NETMASK for $interface from dhcp"
	    ifconfig $interface $NEW_IP $BROADCAST $NETMASK
	    # Get MTU from dhcp server
	    if [ "$mtu" -a "$wan_manual_mtu" = "0" ]; then
		$LOG "Set MTU to $mtu bytes from dhcp server"
		ip link set mtu $mtu dev $interface
	    fi
	fi

    ########################################################################################################
    # DGW/MSROUTES/STATICROUTES/CLASSFULLROUTES/USERROUTES
    ########################################################################################################

	# Get default gateway
	if [ -n "$router" ]; then
	    # default route with metric 0 is through $iface?
	    dgw_otherif=`ip route | grep "default" | grep -v "dev $interface " | sed 's,.*dev \([^ ]*\) .*,\1,g'`
	    if [ -z "$dgw_otherif" ]; then
		# if ip not changed not need delete old default route
		# this is workaroud for ppp used tunnels up over not default routes
		if [ "$FULL_RENEW" = "1" -a "$REPLACE_DGW" = "1" ]; then
		    $LOG "Deleting default route dev $interface"
		    while ip route del default dev $interface ; do
			:
		    done
		fi

		# always parse router variable
		metric=0
		for i in $router ; do
		    $LOG "Add route $i/32:0.0.0.0 dev $interface metric $metric to route list."
		    ROUTELIST_FGW="$ROUTELIST_FGW $i/32:0.0.0.0:$interface:"
		    if [ "$REPLACE_DGW" = "1" ]; then
			$LOG "Add default:$i:$interface:$metric to route dgw list"
			ROUTELIST_DGW="$ROUTELIST_DGW default:$i:$interface:$metric"
			# save first dgw with metric=0 to use in corbina hack
			if [ "$metric" = "0" ]; then
			    echo $i > /tmp/default.gw
			    first_dgw="$i"
			fi
		    fi
    		    metric=`expr $metric + 1`
		done
	    fi
	fi

	# classful routes
	if [ -n "$routes" ]; then
	    for i in $routes; do
		NW=`echo $i | sed 's,/.*,,'`
		GW=`echo $i | sed 's,.*/,,'`
		shift 1
		MASK=`echo $NW | awk '{w=32; split($0,a,"."); \
		    for (i=4; i>0; i--) {if (a[i]==0) w-=8; else {\
		    while (a[i]%2==0) {w--; a[i]=a[i]/2}; break}\
		    }; print w }'`
		if [ "$GW" = "0.0.0.0" -o -z "$GW" ]; then
		    ip route replace $NW/$MASK dev $interface
		else
		    ROUTELIST="$ROUTELIST $NW/$MASK:$GW:$interface:"
		fi
	    done
	fi

	# MSSTATIC ROUTES AND STATIC ROUTES (rfc3442)
	ROUTES="$staticroutes $msstaticroutes"

	if [ "$ROUTES" != " " ]; then
	    set $ROUTES
	    while [ -n "$1" ]; do
		NW="$1"
		GW="$2"
		shift 2
		if [ "$GW" = "0.0.0.0" -o -z "$GW" ]; then
		    ip route replace $NW dev $interface
		else
		    ROUTELIST="$ROUTELIST $NW:$GW:$interface:"
		fi
	    done
	fi

	# first add stub for routesm next add static routes and
	# default gateways need replace/add at end route parces
	# replace dgw must be replaced only if ip selected
	if [ "$REPLACE_DGW" = "1" -a "$FULL_RENEW" = "1" ]; then
	    ROUTELIST="$ROUTELIST_FGW $ROUTELIST $ROUTELIST_DGW"
	    $LOG "Apply route list. And replace DGW."
	else
	    ROUTELIST="$ROUTELIST_FGW $ROUTELIST"
	    $LOG "Apply route list without modify DGW."
	fi
	# aplly parsed route
	for i in `echo $ROUTELIST | sed 's/ /\n/g' | sort | uniq`; do
		IPCMD=`echo $i|awk '{split($0,a,":"); \
		    printf " %s via %s dev %s", a[1], a[2], a[3]; \
		    if (a[4]!="") printf " metric %s", a[4]}'`
		ip route replace $IPCMD
	done
	# add routes configured in web
	if [ -f /etc/routes_replace ]; then
	    $LOF "Apply user routes."
	    /etc/routes_replace replace $lan_if $interface
	fi
	# Add route to multicast subnet
	if [ "$igmpEnabled" = "1" -o "$UDPXYMode" != "0" ]; then
	    $LOG "Add route to multicast subnet."
	    ip route replace "$mcast_net" dev "$interface"
	fi

    ########################################################################################################
    # DNS/WINS and services
    ########################################################################################################

        if [ "$FULL_RENEW" = "1" ]; then
	    # Get DNS servers
	    if [ "$wan_static_dns" = "on" ]; then
		$LOG "Use static DNS."
		# resolv.conf allready generated in internet.sh
	    else
		if [ "$dns" ]; then
		    $LOG "Renew DNS from dhcp $dns $domain"
		    rm -f $RESOLV_CONF
        	    # get domain name
    		    [ -n "$domain" ] && echo domain $domain >> $RESOLV_CONF
		    # parce dnsservers
		    for i in $dns ; do
	    		echo nameserver $i >> $RESOLV_CONF
			ROUTE_NS=`ip route get "$i" | grep dev | cut -f -3 -d " "`
			if [ "$ROUTE_NS" != "" -a "$i" != "$first_dgw" ]; then
			    $LOG "Add static route to DNS $ROUTE_NS dev $interface"
			    REPLACE="ip route replace $ROUTE_NS dev $interface"
			    $REPLACE
			fi
		    done
		else
		    $LOG "Server not send DNS. Please manual set DNS in wan config."
		fi
	    fi
	    if [ "$wins" ]; then
		echo "$wins" > $WINS_CONF
		chmod 644 "$WINS_CONF" > /dev/null 2>&1
	    fi
	    # read for all write by root
	    chmod 644 "$RESOLV_CONF" > /dev/null 2>&1
	    # restart some external services depended by wan ip
	    $LOG "Restart needed services"
	    services_restart.sh dhcp
	fi

    ########################################################################################################
    # VPN Logic and workarounds
    ########################################################################################################
	# if dhcp disables restart must from internet.sh
	# this is restart vpn and others if need
    	if [ "$FULL_RENEW" = "1" ]; then
	    # send Cisco Discovery request
	    if [ -f /bin/cdp-send -a -f /etc/scripts/config-cdp.sh ]; then
		/etc/scripts/config-cdp.sh &
	    fi
	    # restart vpn if ip changed and vpn != pppoe
	    # or vpn enabled and not started
	    if [ "$vpnEnabled" = "on" ] && [ "$vpnType" != "0" -o ! -f /var/run/$vpn_if.pid ]; then
		$LOG "Restart vpnhelper.."
		service vpnhelper restart
	    fi
	    $LOG "End renew procedure..."
	fi
	# reanable forward for paranoid users
	echo 1 > "/proc/sys/net/ipv4/conf/$interface/forwarding"
	if [ "$radvdEnabled" = "1" ]; then
	    echo 1 > "/proc/sys/net/ipv6/conf/$interface/forwarding"
	fi
    ;;
esac
