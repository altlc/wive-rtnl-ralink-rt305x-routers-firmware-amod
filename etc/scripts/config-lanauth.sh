#!/bin/sh

SVC_NAME="LanAUTH"
SVC_PATH="/bin/lanauth"

# if app not exist
if [ ! -e $SVC_PATH ]; then
    exit 0
fi

. /etc/scripts/services_functions.sh

start() {
    vpnEnabled=`nvram_get 2860 vpnEnabled vpnType`
    if [ "$vpnEnabled" = "on" -a "$vpnType" = "6" ]; then
	reload
    else
	stop
    fi
}

reload() {
    get_param
    if  [ "$vpnPassword"  != "" -a "$LANAUTH_LVL" != "" ]; then
	if service_is_running; then
	    if [ "$LANAUTH_LVL" = "1" ]; then
		killall -q -USR1 lanauth
	    elif [ "$LANAUTH_LVL" = "2" ]; then
		killall -q -USR2 lanauth
	    else
		stop
	    fi
	else
	    service_message " mode $LANAUTH_LVL"
	    SVC_OPTS="-v 2 -l $LANAUTH_LVL -p $vpnPassword -A 0"
	    SCV_EXTRA="&"
	    service_start
	fi
    fi
}

get_param() {
    eval `nvram_buf_get 2860 vpnPassword LANAUTH_LVL`
}

stop() {
    service_stop
}

case "$1" in
       start)
           start
           ;;

       stop)
           stop
           ;;

       restart)
           stop
           start
           ;;

	reload)
	   reload
	   ;;

       *)
           echo $"Usage: $0 {start|stop|restart|reload}"
           exit 1
esac
