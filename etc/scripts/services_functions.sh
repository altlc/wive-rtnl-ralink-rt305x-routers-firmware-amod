#!/bin/sh
#

service_stop()
{
    if ! service_is_running; then
#	service_message "Service $SVC_NAME not running!"
	return
    fi

    service_stop_message
    kill_process $SVC_PATH
    service_ok_message

}

service_start()
{
    if service_is_running; then
	service_message "Service $SVC_NAME already running!"
	return
    fi
    
    service_start_message

    if [ "$SVC_PRIORITY" != "" ]; then
	SVC_PRIORITY="-N $SVC_PRIORITY"
    fi

    if [ -x /bin/start-stop-daemon ] ; then
	start-stop-daemon -b -q -S $SVC_PRIORITY -x $SVC_PATH -- $SVC_OPTS
    else
	$SVC_PATH $SVC_OPTS $SVC_EXTRA
    fi
    
    if [ $? -eq 0 ] ; then
	service_ok_message
    else
	service_failed_message
	service_message "Service $SVC_NAME  failed to start!"
    fi
}

service_start_message()
{
    service_message "Starting $SVC_NAME..." -n
}

service_stop_message()
{
    service_message "Stopping $SVC_NAME..." -n
}

service_ok_message()
{
    echo "[  OK  ]"
}

service_failed_message()
{
    echo "[FAILED]"
}

service_is_running()
{
    local process=`basename "$SVC_PATH"`
    local pid=`pidof -o %PPID "$process"`
    [ "x$pid" != "x" ] && return 0 || return 1
}

check_running()
{
    if service_is_running; then
	service_message "Service $SVC_NAME already running!"
	exit 0
    fi
}

service_message()
{
    echo $2 $1
    logger -t "$SVC_NAME" $1
}

service_log()
{
    logger -t "$SVC_NAME" $1
}

generate_uuid() {
    num=0
    for i in `seq 1 36`; do
	id=`dd if=/dev/urandom bs=16 count=1 2>/dev/null | hexdump | head -n1 | cut -c9`
	num=`expr $num + 1`
	if [ "$num" = "9" -o "$num" = "19" -o "$num" = "24" ]; then
	    id="-"
	fi
        outid="$outid$id"
    done
    serial=`echo "$outid" | cut -f1 -d-`
    service_message " UUID is $outid , serial is $serial"
}

kill_processes()
{
    for process in $1
    do
	kill_process "$process"
    done
}

kill_process()
{
    local process=`basename "$1"`
    for pid in `pidof -o %PPID "$process"`
    do
	kill_pid "$pid"
    done
}

kill_pid()
{
    local pid=$1
    kill "$pid" > /dev/null 2>&1
    count=0
    while kill -0 "$pid"  > /dev/null 2>&1; do
	if [ "$count" = "4" ]; then
	    kill -9 "$pid"  > /dev/null 2>&1
	    sleep 1
	    return
	fi
	count="$(($count+1))"
	sleep 1
    done
}

default_actions()
{
    case "$1" in
	start)
	    start
	    ;;

	stop)
	    stop
	    ;;

	restart)
	    stop
	    start
	    ;;

	*)
	    echo $"Usage: $0 {start|stop|restart}"
	    exit 1
    esac
}