#!/bin/sh

# This scipt restart needed services

# include global config
. /etc/scripts/global.sh

LOG="logger -t services"
MODE="$1"

$LOG "Restart needed services and scripts. Mode $MODE"

##########################################################
# Always reload shaper and netfilter rules		 #
##########################################################
    service shaper restart
    service iptables restart

##########################################################
# This is services restart always                        #
##########################################################
    service ripd restart
    service zebra restart
    service radvd restart

##########################################################
# Need restart this servieces only:                    	 #
# 1) if not VPN enable                               	 #
# 2) if VPN enable and this scripts called from ip-up	 #
# 3) if restart mode = all				 #
##########################################################
if [ "$MODE" = "pppd" -o "$MODE" = "dhcp" -o "$MODE" = "all" ]; then
    service ddns restart
    service ntp restart
    service miniupnpd restart
    service xupnpd restart
fi

if [ "$MODE" = "dhcp" ]; then
    service igmp_proxy restart
    service udpxy restart
fi

##########################################################
# Need restart this servieces only:			 #
# 1) if call not from ip-up				 #
# 2) if call not from dhcp script			 #
##########################################################
if [ "$MODE" != "pppd" -a "$MODE" != "dhcp" ]; then
    service kext start
    if [ "$MODE" = "misc" ]; then
	# only misc reply
	service snmpd restart
	service inetd restart
    else
	service pppoe-relay restart
	service chillispot restart
    fi

    service lld2d restart
    service parprouted restart
    service dhcp-relay restart
    service igmp_proxy restart
    service udpxy restart
    service transmission restart
    service minidlna restart
    service miniupnpd restart
    service xupnpd restart
    service crontab restart
fi

if [ "$dnsPEnabled" = "1" -o "$wan_static_dns" = "on" ] && [ "$MODE" != "pppd" -a "$MODE" != "dhcp" ]; then
	# if dnsmasq or static dns enabled and mode=!pppd/!dhcp (aplly at web)
	service dhcpd restart
elif [ "$dnsPEnabled" != "1" -a "$wan_static_dns" != "on" ] && [ "$MODE" = "pppd" -o "$MODE" = "dhcp" ]; then
	# if dnsmasq or static dns disabled and mode=pppd/dhcp (renew/reconnect ISP)
	service dhcpd restart
fi
